/**
 * (c) Ruslan N. Marchenko 2021
 *
 * Originally based on HIDAPI example code
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include <hidapi/hidapi.h>

const char *cmds[] = {
	// Set trackball led command: LEDSxxxxx where x is 1 or 0
	"LEDS",
	"OLED",
	"OINV",
	// Set keyboard led command: LITE[0-9]
	"LITE",
	"PWR3",
	"PWR4",
	"UAR0",
	"UAR1",
	"RPRT",
	NULL
};

int main(int argc, char* argv[])
{
	unsigned char buf[11] = {0x0, 'L', 'I', 'T', 'E', '6', 0x0};
	unsigned char val = '1';
	unsigned short did = 0;
	hid_device *handle = NULL;
	int res, i;

	/* MNT Reform KBD
	 * P:  Vendor=03eb ProdID=2042 Rev=00.01
	 * MNT Reform TRB
	 * P:  Vendor=03eb ProdID=2041 Rev=00.01
	 */

	if (argc > 0 && argv[1]) {
		for(res = 0; cmds[res]; res++) {
			for(i = 0; i < 4 && cmds[res][i] == argv[1][i]; i++);
			if(i == 4) {
				did = (res) ? 0x2042 : 0x2041;
				break;
			}
		}
		if(did == 0) {
			printf(
			"Usage: %s [CMD[ARG]]\n"
			"  Where CMD is one of LEDS,OLED,OINV,LITE,PWR3,PWR4,UAR0,UAR1,RPRT\n"
			"    and ARG is an optional argument for command:\n"
			"    * LEDS - where it is a number 0 to 31 setting bitfield for trackball leds\n"
			"    * LITE - where it is a digit 0 to 9 setting brightness of keyboard backlight\n"
			"  Default is LITE6 - when no command is provided\n", argv[0]);
			exit(1);
		}
		if (did == 0x2041) {
			val = atoi(argv[1] + 4);
			buf[1] = 'L';
			buf[2] = 'E';
			buf[3] = 'D';
			buf[4] = 'S';
			buf[5] = (val & 0x01) ? '1':'0';
			buf[6] = (val & 0x02) ? '1':'0';
			buf[7] = (val & 0x04) ? '1':'0';
			buf[8] = (val & 0x08) ? '1':'0';
			buf[9] = (val & 0x10) ? '1':'0';
		} else {
			buf[1] = cmds[res][0];
			buf[2] = cmds[res][1];
			buf[3] = cmds[res][2];
			buf[4] = cmds[res][3];
			val = argv[1][4];
			if(val >= '0' && val <= '9')
				buf[5] = val;
			else
				buf[5] = 0x0;
		}
	} else {
		did = 0x2042;
	}

	printf("Setting val %s on device %x\n", buf+1, did);
	res = hid_init();
	handle = hid_open(0x03eb, did, NULL);
	res = hid_write(handle, buf, 10);
	hid_close(handle);
	res = hid_exit();

	return 0;
}
